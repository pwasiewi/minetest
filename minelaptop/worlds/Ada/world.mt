gameid = minetest
backend = sqlite3
creative_mode = true
enable_damage = true
server_announce = false
load_mod_worldedit = false
load_mod_worldedit_commands = false
load_mod_worldedit_gui = false
load_mod_worldedit_infinity = false
load_mod_worldedit_limited = false
load_mod_worldedit_shortcommands = false
